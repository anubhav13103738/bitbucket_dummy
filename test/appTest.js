//const assert = require('chai').assert;
const expect = require('expect.js');
const app =require('../app.js').sayhello;
const num = require('../app.js');
describe('App', function(){
  it('app should return hello',function(){
    //assert.equal(app(),'hello');
    expect(app()).to.match(/hello/g);
  });
  it('app returns number',function(){
    
    expect(num.returnNum()).to.be.a('number');
  });
});
